import React, { useState } from "react";

import DateFnsUtils from '@date-io/date-fns';

import Button from '@material-ui/core/Button';
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';

// component slider  CAMBIAR RUTA
import Slider from '../../components/ui/silder';





function DatePickerDemo() {

  // recibiendo la fecha y colocandosela al calendario
  const [date, changeDate] = useState(new Date());

  return (

    <>
      {/* container-datepick - es solo un contenedor que  AHI PUEDE IR LA BASE DEL MODAL 
        SE PUEDE QUITAR CONTAINER-DATEPICK - y colocar otro contenedor padre (POP UP)
      */}
      <div className='container-datepick'   
      >
        {/* calendario */}
        <div className='datepick'  >

          {/* calendario left */}
          <div className='calender-container' >

            <h1>Realiza tu cita</h1>

            <div className='img-container' >
              <img src="https://cdn.icon-icons.com/icons2/1508/PNG/512/systemusers_104569.png" alt="imagen"/>
            </div>

            <article>

              <h5>Vivian Ruiz</h5>
              <p> Friday, <br/> <span> Sept 20 </span> </p>
              {/* <p> Friday, <br/> <span> Sept { JSON.stringify(date) } </span> </p> */}

            </article>

          </div>

          {/* calendario right */}
          <MuiPickersUtilsProvider utils={DateFnsUtils} >
            <DatePicker
              className='muipicker'
              autoOk
              orientation="landscape"
              variant="static"
              openTo="date"
              value={date}
              onChange={changeDate}
            />
          </MuiPickersUtilsProvider>

        </div>

        {/* componentes de la parte de abajo */}
        <section className='date-disponible' >

            <section className="date-disponible__title">
                <p>  
                  <CalendarTodayIcon />
                  <span> Horarios disponibles  </span>
                </p>
            </section>
            <Slider />
            <Button variant="contained" className='button' > Agendar </Button>

        </section>

      </div>

    </>
  );
}

export default DatePickerDemo;