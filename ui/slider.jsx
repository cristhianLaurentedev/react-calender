// import 'react-calendar/dist/Calendar.css';  IMPORTART EN EL _APP.JS para los estilos del calendario


import React from 'react';
import { useState } from 'react';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

import styled from '@emotion/styled';  // import styled from 'styled-components';


// esto puedes hacerlo sin emotion
const SliderContainer = styled.div`

    width: 100%;
    height: 5vh;
    box-sizing: border-box;
    position: relative;
    margin: 0;
    padding: 0;
    display: flex;
    align-items: center;
    overflow: hidden;
    i {
        font-size: 2vw;
        transition: 10s;
    }

    #goLeft {
        left: 0;
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        width: 3.5%;
        height: 64vh;
        background: none;
        border: none;
        outline: none;
        transition: 1s;
        .btn-icon {
            &:hover {
                cursor: pointer
            }
        }
    }
    
    #goRight {
        right: 0;
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        width: 5%;
        height: 64vh;
        display: flex;
        align-items: center;
        jusitify-content: center;
        background: none;
        border: none;
        outline: none;
        transition: 1s;
        .btn-icon {
            position: absolute;
            left: 0;

            &:hover {
                cursor: pointer
            }
        }
    }



`;


// esto puedes hacerlo sin emotion
const Slide = styled.div`

    min-width: 100%;
    height: 100%;
    transition: .5s;
    overflow: hidden;

    section {
        display: flex;
        padding: 0 1em;
        height: 100%;
        align-items: center;
        justify-content: space-around;
    }
`;


function Slider (props) {

    let sliderArr = ['1','2','3','4','5'] // la cantidad de sliders que habra

    const [x, setX] = useState(0);

    // last
    const goLeft = () => x === 0 ? setX(-100 * (sliderArr.length -1)) : setX(x + 100); 
    // next
    const goRight = () => x === -100*(sliderArr.length - 1) ? setX(0) : setX(x - 100); 
    


    return (
        <SliderContainer>
            {
                sliderArr.map((item, index) => {
                    return (
                        <Slide key={index} className="slide" style={{transform: `translateX(${x}%)`}} >
                            <section>
                                {/* aqui ira la data traida de la db  el detaller es :' ) como separarlos :'v por cada slider  */}
                                {/* OJO - creo que se tendria que traer la data como paginacion de 5 en 5 - y ya al darle en el boton de next
                                    se traera los demas horarios.... 
                                */}
                                <span> 12:00 </span>
                                <span> 13:00 </span>
                                <span> 14:00 </span>
                                <span> 15:00 </span>
                                <span> 16:00 </span>
                            </section>
                        </Slide>
                    )
                })
            }

            <button 
                id='goLeft'  
                onClick={goLeft} 
            >
                <ArrowBackIosIcon className='btn-icon' />
            </button>
            <button 
                id='goRight' 
                onClick={goRight}
            >   
                <ArrowForwardIosIcon  className='btn-icon' />
            </button>

        </SliderContainer>
    )
}

export default Slider;